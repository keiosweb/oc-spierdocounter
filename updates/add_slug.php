<?php namespace Viamage\SpierdoCounter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/** @noinspection AutoloadingIssuesInspection */
class AddSlug extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_spierdocounter_spierdolenies',
            function (Blueprint $table) {
                $table->string('slug')->after('id')->index()->default('kotele');
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_spierdocounter_spierdolenies',
            function (Blueprint $table) {
                $table->dropColumn('slug');
            }
        );
    }
}
