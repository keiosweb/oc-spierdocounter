<?php namespace Viamage\SpierdoCounter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpierdoleniesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_spierdocounter_spierdolenies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('day');
            $table->integer('level')->default(1);
            $table->string('description')->nullable();
            $table->boolean('apogeum')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_spierdocounter_spierdolenies');
    }
}
