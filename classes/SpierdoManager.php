<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/22/17
 * Time: 1:22 PM
 */

namespace Viamage\SpierdoCounter\Classes;

use Carbon\Carbon;
use October\Rain\Database\Collection;
use Viamage\SpierdoCounter\Models\Spierdolenie;
use Viamage\SpierdoCounter\Repositories\SpierdolenieRepository;
use Viamage\SpierdoCounter\ValueObjects\PeriodCount;

/**
 * Class SpierdoManager
 * @package Viamage\SpierdoCounter\Classes
 */
class SpierdoManager
{

    /**
     * @var SpierdolenieRepository
     */
    public $repo;

    public function __construct()
    {
        $this->repo = \App::make(SpierdolenieRepository::class);
    }

    /**
     * @param string $slug
     * @param string $date
     * @return Spierdolenie
     */
    public function updateWithPost(string $slug, string $date): Spierdolenie
    {
        $model = $this->repo->getSpierdolenieModel($date, $slug);

        if (!$model) {
            $model = new Spierdolenie();
            $model->day = $date;
        }
        $model->level = post('level');
        $model->slug = $slug;
        $model->description = post('description');
        if (post('apogeum') === 'on') {
            $model->apogeum = true;
        }

        $model->save();

        return $model;
    }

    /**
     * @param PeriodCount $valueObject
     * @param string      $slug
     * @param string      $type
     * @param Carbon      $date
     * @return PeriodCount
     */
    public function calculateData(
        PeriodCount $valueObject,
        string $slug,
        string $type = 'year',
        Carbon $date
    ): PeriodCount {
        if ($type === 'year') {
            $dateFrom = $date->copy()->startOfYear();
        } elseif ($type === 'month') {
            $dateFrom = $date->copy()->startOfMonth();
        } else {
            $dateFrom = $date->copy()->startOfWeek();
        }

        $query = $this->repo->getQueryFor($slug, $dateFrom, $date);
        $daysSinceStart = $dateFrom->diffInDays($date) + 1;
        $allEntries = $query->get();
        /** @var Collection $spierdoloneDays */
        $spierdoloneDays = $query->where('level', '>', 0)->get();

        $emptyDays = $daysSinceStart - count($allEntries);
        $valueObject->count = count($spierdoloneDays) + $emptyDays;
        $valueObject->percent = round(($valueObject->count * 100) / $daysSinceStart, 2);
        $valueObject->medium = $this->calculateMedium($spierdoloneDays, $daysSinceStart, $emptyDays);
        $valueObject->empty = $emptyDays;
        $valueObject->total = count($allEntries);

        return $valueObject;
    }

    /**
     * @param Collection $spierdolenia
     * @param int        $count
     * @param int        $empty
     * @return float|int
     */
    private function calculateMedium(Collection $spierdolenia, int $count = 1, int $empty = 0): int
    {
        $levels = [];
        foreach ($spierdolenia as $spierdolenie) {
            $levels[] = $spierdolenie->level;
            if ($spierdolenie->apogeum) {
                $levels[] = 5;
            }
        }
        if (count($spierdolenia) === 0) {
            return 0;
        }

        return round((array_sum($levels) + ($empty * 3)) / $count);
    }

    /**
     * @param string $diagnosis
     * @param PeriodCount $year
     * @param PeriodCount $month
     * @param PeriodCount $week
     * @return string
     */
    public function generateDiagnosis(
        string $diagnosis,
        PeriodCount $year,
        PeriodCount $month,
        PeriodCount $week
    ): string {
        if ($year->empty > $year->total * 0.5 || $month->empty > $month->total * 0.5 || $week->empty > $week->total * 0.5) {
            $diagnosis .= 'Poniższa diagnoza może nie być miarodajna.<br />';
        } else {
            $diagnosis = '';
        }

        if ($year->percent > 60) {
            $diagnosis .= 'Poziom spierdolenia w tym roku jest patologiczny. ';
        } else {
            $diagnosis .= 'Poziom spierdolenia w tym roku jest średni. ';
        }
        if ($month->percent > 60) {
            $diagnosis .= 'Poziom poprawy w tym miesiacu jest patologiczny. ';
        } elseif($month->percent > 30) {
            $diagnosis .= 'Miesiąc jest średni, jak można się opierdalać 1/3 miesiąca?? ';
        } elseif($month->percent > 0){
            $diagnosis .= 'Miesiącu upływa bez tragedii, ale mogłoby być lepiej. ';
        } else {
            $diagnosis .= 'Miesiąc masz bardzo spoko. ';
        }
        if ($week->percent > 60) {
            $diagnosis .= 'Nic nie zrobiłeś w tym tygodniu. Weź się kurwa ogarnij.';
        } elseif($week->percent > 0) {
            $diagnosis .= 'W tym tygodniu nie ma tragedii. Napierdalaj dalej';
        } else {
            $diagnosis .= 'Tydzień narazie wzorowy. Napierdalaj dalej';
        }

        return $diagnosis;
    }
}
