<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/22/17
 * Time: 2:22 PM
 */

namespace Viamage\SpierdoCounter\Repositories;

use Carbon\Carbon;
use October\Rain\Database\Builder;
use Viamage\SpierdoCounter\Models\Spierdolenie;

/**
 * Class SpierdolenieRepository
 * @package Viamage\SpierdoCounter\Repositories
 */
class SpierdolenieRepository
{

    /**
     * @param string $date
     * @param string $slug
     * @return Spierdolenie|null
     */
    public function getSpierdolenieModel(string $date, string $slug): ?Spierdolenie
    {
        return Spierdolenie::where('day', $date)->where('slug', $slug)->first();
    }

    /**
     * @param string $slug
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     * @return Builder
     */
    public function getQueryFor(string $slug, Carbon $dateFrom, Carbon $dateTo): Builder
    {
        $query = Spierdolenie::where('slug', $slug)->where('day', '>=', $dateFrom->startOfDay())->where(
            'day',
            '<=',
            $dateTo->endOfDay()
        );

        return $query;
    }
}