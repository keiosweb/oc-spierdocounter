<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/21/17
 * Time: 8:41 PM
 */

namespace Viamage\SpierdoCounter\ValueObjects;

use Carbon\Carbon;
use Viamage\SpierdoCounter\Classes\SpierdoManager;
use Viamage\SpierdoCounter\Models\Spierdolenie;

/**
 * Class SpierdolenieCounts
 * @package Viamage\SpierdoCounter\ValueObjects
 */
class SpierdolenieCounts
{
    /**
     * @var PeriodCount
     */
    public $year;

    /**
     * @var PeriodCount
     */
    public $month;

    /**
     * @var PeriodCount
     */
    public $week;

    /**
     * @var string
     */
    public $diagnosis = 'BRAK DANYCH WYSTARCZAJĄCYCH DO UDZIELENIA ODPOWIEDZI. <br />';

    /**
     * @var SpierdoManager
     */
    public $manager;

    public $date;

    /**
     * SpierdolenieCounts constructor.
     *
     * @param Carbon $date
     * @param bool   $init
     * @param string $slug
     */
    public function __construct(Carbon $date, bool $init = false, string $slug = '')
    {
        $this->year = new PeriodCount();
        $this->month = new PeriodCount();
        $this->week = new PeriodCount();
        $this->manager = new SpierdoManager();
        $this->date = $date;
        if ($init) {
            $this->loadData($slug);
        }
    }

    /**
     * @param string $slug
     */
    public function loadData(string $slug): void
    {
        $this->year = $this->manager->calculateData($this->year, $slug, 'year', $this->date);
        $this->month = $this->manager->calculateData($this->month, $slug, 'month', $this->date);
        $this->week = $this->manager->calculateData($this->week, $slug, 'week', $this->date);
        $this->diagnosis = $this->manager->generateDiagnosis($this->diagnosis, $this->year, $this->month, $this->week);
    }
}