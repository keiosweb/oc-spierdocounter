<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/21/17
 * Time: 8:47 PM
 */

namespace Viamage\SpierdoCounter\ValueObjects;

/**
 * Class PeriodCount
 * @package Viamage\SpierdoCounter\ValueObjects
 */
class PeriodCount
{
    /**
     * @var int
     */
    public $count;
    /**
     * @var int
     */
    public $percent;
    /**
     * @var int
     */
    public $medium;
    /**
     * @var string
     */
    public $description;
    /**
     * @var int
     */
    public $empty;
    /**
     * @var int
     */
    public $total;
}