<?php namespace Viamage\SpierdoCounter\Models;

use Model;

/**
 * Spierdolenie Model
 */
class Spierdolenie extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_spierdocounter_spierdolenies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
}
