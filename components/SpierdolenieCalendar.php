<?php namespace Viamage\SpierdoCounter\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Viamage\SpierdoCounter\Classes\SpierdoManager;
use Viamage\SpierdoCounter\ValueObjects\SpierdolenieCounts;

/**
 * Class SpierdolenieCalendar
 * @package Viamage\SpierdoCounter\Components
 */
class SpierdolenieCalendar extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'SpierdolenieCalendar Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Slug',
                'description' => 'Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'date' => [
                'title'       => 'Date',
                'description' => 'Date',
                'default'     => '{{ :date }}',
                'type'        => 'string',
            ],
        ];
    }

    /**
     * @var SpierdoManager
     */
    private $manager;

    /**
     * @var Carbon
     */
    private $date;

    /**
     * SpierdolenieCalendar constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->manager = new SpierdoManager();
        $this->date = Carbon::now();
    }

    public function onReplaceBox()
    {
        $date = post('date');
        $this->date = new Carbon($date);
        $this->setupPage();
    }

    /**
     * Init data
     */
    public function onRun(): void
    {
        $this->page['userSlug'] = $this->property('slug');
        $this->setupDate();
        $this->setupPage();
    }

    /**
     * Ajax method for Save
     */
    public function onUpdateSpierdolenie(): void
    {
        if (post('date')) {
            $this->date = new Carbon(post('date'));
        } else {
            $this->setupDate();
        }
        $this->manager->updateWithPost($this->property('slug'), $this->date->toDateString());
        $this->setupPage();
    }

    /**
     * Setup page twig variables
     */
    public function setupPage(): void
    {
        $today = $this->date->toDateString();
        $daysSinceYearStart = $this->date->copy()->startOfYear()->diffInDays($this->date) + 1;
        $daysSinceMonthStart = $this->date->copy()->startOfMonth()->diffInDays($this->date) + 1;
        $daysSinceWeekStart = $this->date->copy()->startOfWeek()->diffInDays($this->date) + 1;
        $this->page['slug'] = ucfirst($this->property('slug'));
        $this->page['today'] = $today;
        $this->page['today_spierdolenie'] = $this->manager->repo->getSpierdolenieModel($today, $this->property('slug'));
        $this->page['days_since_year_start'] = $daysSinceYearStart;
        $this->page['days_since_month_start'] = $daysSinceMonthStart;
        $this->page['days_since_week_start'] = $daysSinceWeekStart;
        $counts = new SpierdolenieCounts($this->date, true, $this->property('slug'));
        $this->page['spierdolenie_counts'] = $counts;
    }

    private function setupDate()
    {
        if ($this->property('date')) {
            try {
                $this->date = Carbon::parse($this->property('date'));
            } catch (\Exception $e) {
                \Flash::error('Invalid Date format');
            }
        }
    }
}
